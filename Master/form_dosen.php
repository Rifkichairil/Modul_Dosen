<?php
	include_once 'top.php';
      //panggil file yang melakukan operasi db

	require_once 'db/class_dosen.php';

	$obj_dosen = new Dosen();
    //buat variabel untuk memanggil class

	$_idedit = $_GET['id'];
    //buat variabel utk menyimpan id

	if(!empty($_idedit)){
		$data = $obj_dosen->findByID($_idedit);
    //buat pengecekan apakah datanya ada atau tidak

	}else{
		$data = [];
	}
?>
<script src="js/form_validasi_dosen.js"></script>

<form class="form-horizontal" method="POST" name="form_dosen" action="proses_dosen.php">
<fieldset><fieldset>

<!-- Form Name -->
<legend>Form Name</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nidn">NIDN</label>  
  <div class="col-md-4">
  <input id="nidn" name="nidn" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['nidn']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Nama</label>  
  <div class="col-md-4">
  <input id="nama" name="nama" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['nama']?>">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="gelar_belakang">Gelar Belakang</label>  
  <div class="col-md-4">
  <input id="gelar_belakang" name="gelar_belakang" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['gelar_belakang']?>">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="jk">Jenis Kelamin</label>
  <div class="col-md-4">
    <select id="jk" name="jk" class="form-control">
      <option value="0">Pilih </option>
      <option value="L">Laki-Laki</option>
      <option value="P">Perempuan</option>
   <!--   <option value="3">FK</option>
      <option value="4">FB</option>
      <option value="5">MA</option>
      <option value="6">H</option>
-->
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="prodi_id">Prodi</label>
  <div class="col-md-4">
    <select id="prodi_id" name="prodi_id" class="form-control">
      <option value="0">Pilih Prodi</option>
      <option value="1">SI</option>
      <option value="2">TI</option>
   <!--   <option value="3">FK</option>
      <option value="4">FB</option>
      <option value="5">MA</option>
      <option value="6">H</option>
-->
    </select>
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="jabatan_id">Jabatan</label>
  <div class="col-md-4">
    <select id="jabatan_id" name="jabatan_id" class="form-control">
      <option value="0">Pilih </option>
      <option value="1">Tenaga Pengajar</option>
      <option value="2">Asisten ahli</option>
      <option value="3">Lektor</option>
      <option value="4">Lektor Ahli</option>
      <option value="5">Professor</option>
   <!--   <option value="3">FK</option>
      <option value="4">FB</option>
      <option value="5">MA</option>
      <option value="6">H</option>
-->
    </select>
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="pend_akhir">Pendidikan Akhir</label>  
  <div class="col-md-4">
  <input id="pend_akhir" name="pend_akhir" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['pend_akhir']?>">
    
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">

  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>

    <?php

    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>

    <?php

    }?>
<script src="js/form_validasi_dosen.js"></script>

  </div>
</div>

</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>
