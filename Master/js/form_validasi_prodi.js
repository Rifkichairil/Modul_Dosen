$(function(){
	$("form[name='form_prodi']").validate({
		rules:{
			kode:{
				required:true,
				maxlenght: 3,
			},
			nama:"required",
		},
		messages: {
			kode:{
				required:"Tolong diisi",
				maxlenght:"cuma 3"
			},
			nama:"Tolong diisi jangan sampe kosong",
		},
		submitHandler: function)(form){
			form.submit();
		}
	});
});