<?php
    include_once 'top.php';
    //panggil file yang melakukan operasi db
    require_once 'db/class_rombel.php';
    //buat variabel untuk memanggil class
    $obj_rombel = new Rombel();
    //buat variabel utk menyimpan id
    $_idedit = $_GET['id'];
    //buat pengecekan apakah datanya ada atau tidak
    if(!empty($_idedit)){
        $data = $obj_rombel->findByID($_idedit);
    }else{
        $data = [];
    }
?>
<script src="js/form_validasi_rombel.js"></script>

<form name="form_rombel" class="form-horizontal" method="POST" action="proses_rombel.php">
<fieldset>

<!-- Form Name -->
<legend>Form Rombel</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="nama">Nama Rombel</label>  
  <div class="col-md-4">
  <input id="nama" name="nama" type="text" placeholder="" class="form-control input-md" value="<?php echo $data['nama']?>">
    
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="mhs_angkatan">Mahasiswa Angkatan</label>
  <div class="col-md-4">
    <select id="mhs_angkatan" name="mhs_angkatan" class="form-control">
      <option value="0">Pilih Tahun Angkatan</option>
      <option value="1">2016</option>
      <option value="2">2017</option>
    </select>
  </div>
</div>


<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="dosen_pa">Dosen Pembimbing</label>
  <div class="col-md-4">
    <select id="dosen_pa" name="dosen_pa" class="form-control">

      <option value="0">Pilih Dosen Pembiming</option>
      <option value="1">AHMAD RIO ADRIANSYAH</option>
      <option value="2">AMALIA RAHMAH</option>
      <option value="3">BACHTIAR FIRDAUS</option>
      <option value="4">BAMBANNG PRIANTIONO</option>
      <option value="5">HENTY SAPTONO</option>
      <option value="6">HILMY ABIDZAR TAWAKAL</option>
      <option value="7">INDRA HERMAWAN</option>
      <option value="8">KURNIAWAN DWI PRASETYO</option>
      <option value="9">LUKMAN ROSYADI</option>
      <option value="10">MUHAMMAD BINTANG</option>
      <option value="11">NUGROHO DWI SAPUTRA</option>
      <option value="12">REZA ALDIANSYAH</option>
      <option value="13">REZA PRIMARDIANSYAH</option>
      <option value="14">RUSMANTO</option>
      <option value="15">SALMAN EL FARISI</option>
      <option value="16">SAPTO WALUYO</option>
      <option value="17">SIROJUL MUNIR</option>
      <option value="18">SUHENDI</option>
      <option value="19">WARSONO</option>
      <option value="20">ZAKI IMADUDDIN</option>
    </select>
  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="prodi_id">Prodi</label>
  <div class="col-md-4">
    <select id="prodi_id" name="prodi_id" class="form-control">
      <option value="0">Pilih Prodi</option>
      <option value="1">SI</option>
      <option value="2">TI</option>
   <!--   <option value="3">FK</option>
      <option value="4">FB</option>
      <option value="5">MA</option>
      <option value="6">H</option>
-->
    </select>
  </div>
</div>

<!-- Button (Double) -->
<div class="form-group">
  <label class="col-md-4 control-label" for="proses"></label>
  <div class="col-md-8">
  <?php
    if(empty($_idedit)){
    ?>
      <input type="submit" name="proses" class="btn btn-success" value="Simpan"/>
    <?php
    }else{
      ?>
      <input type="hidden" name="idedit" value="<?php echo $_idedit?>"/>
      <input type="submit" name="proses" class="btn btn-primary" value="Update"/>
      <input type="submit" name="proses" class="btn btn-danger" value="Hapus"/>
    <?php
    }?>
  </div>
</div>
</fieldset>
</form>

<?php
    include_once 'bottom.php';
?>

