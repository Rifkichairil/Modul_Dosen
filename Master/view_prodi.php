<?php
    include_once 'top.php';
        //panggil file untuk operasi prodi

    require_once 'db/class_prodi.php';
        //buat variabel utk menyimpan id

    $obj_prodi = new Prodi();
        //buat variabel untuk mengambil id

    $_id = $_GET['id'];
    $data = $obj_prodi->findByID($_id);
?>
<div class="row">
	<div class="col-md-12">
 		<div class="panel panel-default">
 			<div class="panel-heading">
 				<h3 class="panel-title">View Prodi</h3>
 			</div>
 	
 		<div class="panel-body">
 			<table class="table">
			 <tr>
 			 	<td class="active">Kode</td>
 			 	<td>:</td>
 			 	<td> <?php echo $data['kode']?> </td>
 			 </tr>
  			 
  			 <tr>
 				<td class="active">Nama Prodi</td>
 				<td>:</td>
 				<td><?php echo $data['nama']?></td>
			 </tr>
  			 
 			</table>
 		</div>
 
 	<div class="panel-footer">
 		<a class="btn icon-btn btn-success" href="form_prodi.php">
 			<span class="glyphicon btn-glyphicon glyphicon-plus imgcircle text-success"></span>
		 Tambah Prodi
		</a>
 
             	  </div>
 			</div>
	  </div>
</div>


<?php
	include_once 'bottom.php';
?>