<?php
	include_once 'top.php';

/*
	mysql> desc dosen;
+----------------+-------------+------+-----+---------+----------------+
| Field          | Type        | Null | Key | Default | Extra          |
+----------------+-------------+------+-----+---------+----------------+
| id             | int(11)     | NO   | PRI | NULL    | auto_increment |
| nidn           | varchar(10) | YES  |     | NULL    |                |
| nama           | varchar(45) | YES  |     | NULL    |                |
| gelar_depan    | varchar(6)  | YES  |     | NULL    |                |
| gelar_belakang | varchar(15) | YES  |     | NULL    |                |
| tmp_lahir      | varchar(30) | YES  |     | NULL    |                |
| tgl_lahir      | date        | YES  |     | NULL    |                |
| jk             | char(1)     | YES  |     | NULL    |                |
| prodi_id       | int(11)     | NO   | MUL | NULL    |                |
| email          | varchar(45) | YES  |     | NULL    |                |
| jabatan_id     | int(11)     | NO   | MUL | NULL    |                |
| pend_akhir     | varchar(4)  | YES  |     | NULL    |                |
+----------------+-------------+------+-----+---------+----------------+
*/

require_once "DAO.php";

class Dosen extends DAO{

	public function __construct(){
		parent::__construct("dosen");
	}

	public function simpan($data){
		$sql = "INSERT INTO ".$this->tableName.
		" (id, nidn, nama, gelar_belakang, jk, prodi_id, jabatan_id, pend_akhir)".
		" VALUES (default,?,?,?,?,?,?,?)";

		$ps = $this->koneksi->prepare($sql);
		$ps->execute($data);
			return $ps->rowCount();
	}

	public function ubah($data){
		$sql = "UPDATE ".$this->tableName.
		" SET nidn=?, nama=?, gelar_belakang=?, jk=?, prodi_id=?, jabatan_id=?, pend_akhir=?".
		" WHERE id=?";

		$ps = $this->koneksi->prepare($sql);
		$ps->execute($data);
			return $ps->rowCount();
	}

        public function getStatistik(){
            $sql = "SELECT a.nama,COUNT(b.id) as jumlah from prodi a
                    LEFT JOIN dosen b ON a.id=b.prodi_id
                    GROUP BY a.nama";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute();
                return $ps->fetchAll();
        }


}
?>