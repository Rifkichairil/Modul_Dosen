<?php
    include_once 'top.php';

/*
mysql> desc rombel;
+--------------+-------------+------+-----+---------+----------------+
| Field        | Type        | Null | Key | Default | Extra          |
+--------------+-------------+------+-----+---------+----------------+
| id           | int(11)     | NO   | PRI | NULL    | auto_increment |
| nama         | varchar(45) | YES  |     | NULL    |                |
| mhs_angkatan | int(11)     | YES  |     | NULL    |                |
| dosen_pa     | int(11)     | NO   | MUL | NULL    |                |
| prodi_id     | int(11)     | NO   | MUL | NULL    |                |
+--------------+-------------+------+-----+---------+----------------+
5 rows in set (0.01 sec)
*/
 require_once "DAO.php";
    class Rombel extends DAO
    {
        public function __construct()
        {
            parent::__construct("rombel");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id,nama,mhs_angkatan,dosen_pa,prodi_id) ".
            " VALUES (default,?,?,?,?)";

            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            	return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET nama=?,mhs_angkatan=?,dosen_pa=?,prodi_id=?".
            " WHERE id=?";

            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            	return $ps->rowCount();
        }
        //buat fungsi untuk menampilkan statistik


    }
?>
<?php
    include_once 'botton.php';
?>
<!--
        public function getStatistik(){
            $sql = "SELECT a.nama,COUNT(b.id) as jumlah from kategori a
                    LEFT JOIN kegiatan b ON a.id=b.kategori_id
                    GROUP BY a.nama";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute();
                return $ps->fetchAll();
        }
-->